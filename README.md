# STM32F0xx HAL Driver

The STM32Cube hardware abstraction layer (HAL) and Low Layer APIs (LL) for STM32F0 family with CMSIS library. 

**This is reduced version of otherwise bulky library which comes with STM32CubeMX software.** 

This library is compatible with [PlatformIO Library Management System](https://docs.platformio.org/en/latest/librarymanager/creating.html).

For use with platformio you can add library in platformio.ini like this:
>[env]
>platform = ststm32
>
>lib_deps = 
>
>&nbsp;&nbsp;&nbsp;&nbsp;stm32f0xx_hal_driver = https://gitlab.com/vojtech.vigner/stm32f0xx_hal_driver.git

Don't forget to select proper linker script if not selected by *board* variable. 
>board_build.ldscript = "mylinker.ld"

Add startup code to your project, startup examples are located in templates/*compiler* folder. These startup examples calls *SystemInit* function before *main*. 

*SystemInit* is used to setup CPU clocks and configure Interrupt Vector Table. Again examples are located in *templates/system* folder.

Code location in STM32CubeMX library folder:
- **include**: *Drivers/STM32F0xx_HAL_Driver/Inc*
- **include/cmsis**: *Drivers/CMSIS/Include*
- **include/device**: *Drivers/CMSIS/Device/ST/STM32F0xx/Include*
- **src**: *Drivers/STM32F0xx_HAL_Driver/Src*
- **templates**: *Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_\*_template.c*
- **templates/arm**: *Drivers/CMSIS/Device/ST/STM32F0xx/Source/Templates/arm*
- **templates/gcc**: *Drivers/CMSIS/Device/ST/STM32F0xx/Source/Templates/gcc*
- **templates/iar**: *Drivers/CMSIS/Device/ST/STM32F0xx/Source/Templates/iar*
- **templates/system**: *Drivers/CMSIS/Device/ST/STM32F0xx/Source/Templates*

# Original STM README:

# STM32CubeF0 MCU Firmware Package

**STM32Cube** is an STMicroelectronics original initiative to ease the developers life by reducing efforts, time and cost.

**STM32Cube** covers the overall STM32 products portfolio. It includes a comprehensive embedded software platform (this repo), delivered for each series (such as the STM32CubeF0 for the STM32F0 series).
   * The CMSIS modules (core and device) corresponding to the ARM-tm core implemented in this STM32 product
   * The STM32 HAL-LL drivers : an abstraction drivers layer, the API ensuring maximized portability across the STM32 portfolio 
   * The BSP Drivers of each evaluation or demonstration board provided by this STM32 series 
   * A consistent set of middlewares components such as RTOS, USB, FatFS, TCP/IP, Graphics ...
   * A full set of software projects (basic examples, applications or demonstrations) for each board provided by this STM32 series
   
The **STM32CubeF0 MCU Package** projects are directly running on the STM32F0 series boards. You can find in each Projects/*Board name* directories a set of software projects (Applications/Demonstration/Examples)

Please take a look to the Release Note document to get detailed information on the STM32F0 features 

In this FW Package, the module **Middlewares/ST/STemWin** is not directly accessible. It must be downloaded from a ST server, the URL is available in a readme.txt file inside the module.

## Boards available
  * STM32F0 
    * [32F0308DISCOVERY](https://www.st.com/en/evaluation-tools/32f0308discovery.html)
    * [32F072BDISCOVERY](https://www.st.com/en/evaluation-tools/32f072bdiscovery.html)
	* [NUCLEO-F030R8](https://www.st.com/en/evaluation-tools/nucleo-f030r8.html)
	* [NUCLEO-F031K6](https://www.st.com/en/evaluation-tools/nucleo-f031k6.html)
	* [NUCLEO-F042K6](https://www.st.com/en/evaluation-tools/nucleo-f042k6.html)
	* [NUCLEO-F070RB](https://www.st.com/en/evaluation-tools/nucleo-f070rb.html)
	* [NUCLEO-F072RB](https://www.st.com/en/evaluation-tools/nucleo-f072rb.html)
	* [NUCLEO-F091RC](https://www.st.com/en/evaluation-tools/nucleo-f091rc.html)
	* [STM32072B-EVAL](https://www.st.com/en/evaluation-tools/stm32072b-eval.html)
	* [STM32091C-EVAL](https://www.st.com/en/evaluation-tools/stm32091c-eval.html)
	* [STM32F0DISCOVERY](https://www.st.com/en/evaluation-tools/stm32f0discovery.html)

## Troubleshooting

**Caution** : The **Issues** requests are strictly limited to submit problems or suggestions related to the software delivered in this repo 

**For any question** related to the STM32F0 product, the hardware performance, the hardware characteristics, the tools, the environment, you can submit a topic on the [ST Community/STM32 MCUs forum](https://community.st.com/s/group/0F90X000000AXsASAW/stm32-mcus)
